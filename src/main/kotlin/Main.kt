fun main() {
    val p = Point(1, 1)
    val cloud = CloudOfPoints(arrayListOf(Point(1, 1)))

    p.move(1, 1); cloud.move(2, 2)

    val p2 = cloud.points[0]

    println("${p.x} ${p.y}")
    println("${p2.x} ${p2.y}")
}