interface IMovable {
    fun move( dx: Int, dy: Int)
}
